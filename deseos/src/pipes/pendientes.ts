import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../class/index';

@Pipe({
  name: 'pendientes',
  pure: false
})
export class PendientesPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform( listas:Lista[],estado:boolean=false):Lista[] {
    //return value.toLowerCase();
    let nuevalista:Lista[] = [];

    for(let lista of listas) {
      if(lista.terminada == estado) {
        nuevalista.push( lista );
      }
    }
    return nuevalista;
  }
}
