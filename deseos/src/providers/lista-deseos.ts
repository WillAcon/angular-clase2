import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Lista } from '../class/listas';

@Injectable()
export class ListaDeseosService {
  listas:Lista[] = [];
  constructor() {
    this.cargarData();
    console.log('Hello ListaDeseos Provider');
  }

  updateData() {
    localStorage.setItem( "data", JSON.stringify(this.listas) );
  }

  cargarData() {
    if(localStorage.getItem("data")) {
        this.listas = JSON.parse(localStorage.getItem("data"));
    }
  }

  agregarLista( lista:Lista) {
    this.listas.push(lista);
    this.updateData();
  }

  eliminarLista( index:number) {
    this.listas.splice(index,1);
    this.updateData();
  }

}
