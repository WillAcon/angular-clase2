import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ListaDeseosService } from '../../providers/lista-deseos';
import { DetallePage } from '../detalle/detalle';

@Component({
  selector: 'page-terminados',
  templateUrl: 'terminados.html',
})
export class TerminadosPage {

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private _listaDeseos: ListaDeseosService
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Terminados');
  }

  verDetalle(lista, index) {
    this.navCtrl.push(DetallePage, {lista,index});
  }

}
