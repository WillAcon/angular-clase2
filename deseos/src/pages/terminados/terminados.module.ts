import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TerminadosPage } from './terminados';

@NgModule({
  declarations: [
    TerminadosPage,
  ],
  imports: [
    IonicPageModule.forChild(TerminadosPage),
  ],
  exports: [
    TerminadosPage
  ]
})
export class TerminadosModule {}
