import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ListaDeseosService } from '../../providers/lista-deseos';
import { AgregarPage } from '../agregar/agregar';
import { DetallePage } from '../detalle/detalle';

@Component({
  selector: 'page-pendientes',
  templateUrl: 'pendientes.html',
})
export class PendientesPage {

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private _listaDeseos: ListaDeseosService
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Pendientes');
  }
  irAgregar() {
    this.navCtrl.push(AgregarPage);
  }

  verDetalle(lista, index) {
    this.navCtrl.push(DetallePage, {lista,index});
  }
}
