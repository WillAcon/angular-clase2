import { Component } from '@angular/core';
import { AlertController ,NavController, NavParams } from 'ionic-angular';

import { ListaItem, Lista} from '../../class/index';

import { ListaDeseosService } from '../../providers/lista-deseos';

@Component({
  selector: 'page-agregar',
  templateUrl: 'agregar.html',
})
export class AgregarPage {
  nombreLista:string = '';
  nombreItem:string = '';

  items:ListaItem[] = [];

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     public _listaDeseos: ListaDeseosService
   ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Agregar');
  }

  agregar(){
    if(this.nombreItem.length == 0) {
      return;
    }

    let item = new ListaItem();
    item.nombre = this.nombreItem;
    this.items.push(item);
    this.nombreItem = '';
  }

  eliminar(index) {
    this.items.splice(index, 1);
  }

  guardarLista(){
    if(this.nombreLista.length == 0) {
      let alert = this.alertCtrl.create({
        title: 'Nombre de la lista',
        subTitle: 'El nombre de la lista es necesario',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    let lista = new Lista(this.nombreLista);
    lista.items = this.items;

    //this._listaDeseos.listas.push( lista );
    this._listaDeseos.agregarLista( lista );
    this.navCtrl.pop();
  }


  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }

}
