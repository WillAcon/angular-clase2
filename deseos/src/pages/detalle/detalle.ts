import { Component } from '@angular/core';
import { AlertController,  NavController, NavParams } from 'ionic-angular';

import { Lista, ListaItem} from '../../class/index';
import { ListaDeseosService } from '../../providers/lista-deseos';

@Component({
  selector: 'page-detalle',
  templateUrl: 'detalle.html',
})
export class DetallePage {
  index:number;
  lista:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public _listaDeseos: ListaDeseosService
   ) {
     console.log(this.navParams);

     this.index = this.navParams.get("index");
     this.lista = this.navParams.get("lista");

   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Detalle');
  }

  actualizar(item:ListaItem) {
    item.completado = !item.completado;

    let todosMarcados = true;
    for(let item of this.lista.items) {
      if(!item.completado) {
        todosMarcados = false;
        break;
      }
    }

    this.lista.terminada = todosMarcados;
    this._listaDeseos.updateData();

  }

  borrarItem() {
    let confirm = this.alertCtrl.create({
      title: this.lista.nombre,
      message: '¿Esta seguro que desea borrar la lista?',
      buttons: ['Cancelar',
        {
          text: 'Eliminar',
          handler: () => {
            //console.log('Agree clicked');
            this._listaDeseos.eliminarLista(this.index);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }



}
