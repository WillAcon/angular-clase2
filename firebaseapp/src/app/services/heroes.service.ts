import { Injectable } from '@angular/core';

import { Http, Headers } from '@angular/http';
import { Heroe } from '../interfaces/heroe';

import  'rxjs/Rx';

@Injectable()
export class HeroesService {

  heroesURL:string = "https://forumapp-7f9bf.firebaseio.com/heroes.json";
  heroeURL:string = "https://forumapp-7f9bf.firebaseio.com/heroes/";

  constructor(
    private http:Http

  ) { }

  nuevoHeroe( heroe: Heroe) {
    let body = JSON.stringify( heroe );
    let headers = new Headers({
      'Content-Type':'application/json'
    });


    return this.http.post( this.heroesURL, body, { headers })
                    .map( res=> {
                      console.log(res.json());
                      return res.json();
                    })
  }
  actualizarHeroe( heroe: Heroe, key$:string ) {
      let body = JSON.stringify( heroe );
      let headers = new Headers({
        'Content-Type':'application/json'
      });

      let url = `${ this.heroeURL }/${ key$ }.json`;

      return this.http.put( url , body, { headers })
                      .map( res=> {
                        console.log(res.json());
                        return res.json();
                      })
  }

  getHeroe( key$:string ) {
    //  let body = JSON.stringify( heroe );
      let headers = new Headers({
        'Content-Type':'application/json'
      });

      let url = `${ this.heroeURL }/${ key$ }.json`;

      return this.http.get( url , { headers })
                      .map( res=> {
                        console.log(res.json());
                        return res.json();
                      })
  }

  getHeroes( ) {
    //  let body = JSON.stringify( heroe );
      let headers = new Headers({
        'Content-Type':'application/json'
      });

      return this.http.get( this.heroesURL , { headers })
                      .map( res=> {
                        return res.json();
                      })
  }
  eliminarHeroe( key$:string ) {
    //  let body = JSON.stringify( heroe );
      let headers = new Headers({
        'Content-Type':'application/json'
      });
      let url = `${ this.heroeURL }/${ key$ }.json`;

      return this.http.delete( url , { headers })
                      .map( res=> {
                        return res.json();
                      })
   }

}
