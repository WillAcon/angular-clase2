import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
//import { Heroe } from '../../interfaces/heroe';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent implements OnInit {

  heroes:any[] = [];
  loading:boolean = true;

  constructor(
    private _heroeService:HeroesService,
    private router:Router
  //  private route: ActivatedRoute
  ) {

      this._heroeService.getHeroes()
            .subscribe( data => {

              // for(let key$ in data ){
              //   let h = data[key$];
              //   h.key$ = key$;
              //   this.heroes.push(data[key$]);
              // }

              this.heroes = data;
              this.loading = false;
              console.log(this.heroes);
            })
    }

  ngOnInit() {
  }

  borrarHeroe( key$:string ) {
     this._heroeService.eliminarHeroe(key$)
          .subscribe( respuesta => {
            if(respuesta){
              console.log(respuesta);
            }else {
              delete this.heroes[key$];
            }

          })

  }

}
