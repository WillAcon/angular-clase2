import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys',
  pure: false
})
export class KeysPipe implements PipeTransform {

  transform(value: any): any {
    //return Object.keys(value);

   //console.log(Object.keys(value));
   
    let keys = [];
    for(let key in value) {
      keys.push(key);
    }

    return keys;

 }
}
