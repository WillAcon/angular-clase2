import { FireUploadPage } from './app.po';

describe('fire-upload App', () => {
  let page: FireUploadPage;

  beforeEach(() => {
    page = new FireUploadPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
