import { Component, OnInit } from '@angular/core';
import { FileItemModule } from '../../models/file-item.module';
import { CargaImagenesService } from '../../services/carga-imagenes.service';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styles: []
})
export class CargaComponent implements OnInit {

  estaSobreDropZone:boolean = false;
  permiteCargar:boolean = true;
  archivos:FileItemModule[] = [];

  constructor( public _cargaImagenes: CargaImagenesService ) { }

  ngOnInit() {
  }

  archivoSobreDropZone(e:boolean){
    this.estaSobreDropZone = e;
  }
  cargarImagenesFirebase() {
    this.permiteCargar = false;
    this._cargaImagenes.SubirImagenes(this.archivos);
  }
  limpiarArchivos(){
    this.archivos = [];
    this.permiteCargar = true;
  }

}
