import { Directive, EventEmitter, ElementRef, HostListener,
          Input, Output} from '@angular/core';

import { FileItemModule } from '../models/file-item.module';

@Directive({
  selector: '[NgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() archivos:FileItemModule[] = [];
  @Output() archivoSobre: EventEmitter<any> = new EventEmitter();

  constructor( public elemento:ElementRef ) { }

  @HostListener('dragenter', ['$event'])
  public onDragEnter(event:any){
    this.archivoSobre.emit( true );
  }
  @HostListener('dragleave', ['$event'])
  public onDragLeave(event:any){//nombre opcional
    this.archivoSobre.emit( false );
  }
  @HostListener('dragover', ['$event'])
  public onDragOver(event:any){//nombre opcional
    let transferencia = this._getTransferencia( event );
    transferencia.dropEffect = "copy";
    this._prevenirDetener( event );

    this.archivoSobre.emit( true );
  }
  @HostListener('drop', ['$event'])
  public onDrop(event:any){//nombre opcional

    let transferencia = this._getTransferencia( event );
    if(!transferencia) {
      return;
    }
    this._agregarArchivos( transferencia.files);
    this.archivoSobre.emit( false );
    this._prevenirDetener( event );

  }

  private _getTransferencia( event:any ){
    //console.log(event);
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _agregarArchivos(archivosLista:FileList) {
    //  console.log(archivosLista);
      for( let propiedad in Object.getOwnPropertyNames(archivosLista) ) {
        let archTemp = archivosLista[propiedad];
        if( this._archivoPuedeSerCargado(archTemp) ) {
          let newFile = new FileItemModule(archTemp);
          this.archivos.push( newFile );
        }
      }

      console.log(this.archivos);
  }
  private _prevenirDetener( event:any ){
    event.preventDefault();
    event.stopPropagation();
  }

  private _archivoPuedeSerCargado( archivo:File) {
     if(!this._archivoFueDroppeado( archivo.name ) && this._esImagen(archivo.type) ) {
       return true;
     }
     return false;
  }

  private _archivoFueDroppeado(nombreArchivo:string):boolean{
    for(let i in this.archivos){
      let arch = this.archivos[i];
      if(arch.archivo.name === nombreArchivo ){
        console.log("el archivo ya existe e na lista", nombreArchivo);
        return true;
      }
    }

    return false;
  }

  private _esImagen( tipoArchivo: string ):boolean {
      return (tipoArchivo == '' || tipoArchivo == undefined )? false:tipoArchivo.startsWith('image');
  }



}
