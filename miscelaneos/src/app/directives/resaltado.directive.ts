import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {


  constructor( private el:ElementRef) {
    //el.nativeElement.style.backgroundColor = "red";
    console.log("directiva llamada");
  }

  @Input("appResaltado") nuevoColor:any;

  @HostListener("mouseenter") mouseEntro() {
    this.resaltar(this.nuevoColor.color || "red");
    this.tamanio(this.nuevoColor.size || "15px");
    //this.el.nativeElement.style.backgroundColor = "blue";
  }
  @HostListener("mouseleave") mouseSalio() {
    this.resaltar(null);
    this.tamanio(null);
    //this.el.nativeElement.style.backgroundColor = null;
  }

  private resaltar(color:string){
    this.el.nativeElement.style.backgroundColor = color;
  }
  private tamanio(size:string){
    this.el.nativeElement.style.fontSize = size;
  }

}
