import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styles: [
    `
      .ng-invalid.ng-touched:not(form) {
        border: 1px solid red;
      }
     `
  ]
})
export class TemplatesComponent implements OnInit {

  usuario:Object = {
    nombre:"",
    apellido:"acuna",
    correo:'demo@demo.com',
    pais:'PE',
    sexo:"Hombre",
    acepta:false
  };

  paises = [
  {
    codigo:"CRI",
    nombre:"Costa Rica"
  },
  {
    codigo:"PE",
    nombre:"Perú"
  },
  {
    codigo:"US",
    nombre:"Estados unidos"
  },
  {
    codigo:"PA",
    nombre:"Panamá"
  }

];
sexos:string[] = ["Hombre","Mujer"];

  constructor() { }

  ngOnInit() {
  }

  guardar(forma:NgForm){
    console.log("formulario NgForm");
    console.log(forma);

    console.log("formulario form");
    console.log(forma.value);

    console.log("usuario form");
    console.log(this.usuario);

  }

}
