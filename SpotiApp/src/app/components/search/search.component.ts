import { Component, OnInit } from '@angular/core';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {
  termino:string = "";
  constructor(
    private _SpotifyService:SpotifyService
  ) { }

  ngOnInit() {

  }

  buscarArtista(){
    this._SpotifyService.getArtistas(this.termino)
    .subscribe();
    //console.log(this.termino);
  }

}
