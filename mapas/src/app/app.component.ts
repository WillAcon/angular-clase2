import { Component } from '@angular/core';
import { MapasService } from './services/mapas.service';
import { Marcador } from './interfaces/marcador';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  lat: number = -12.054881;
  lng: number = -77.036857;
  zoom:number = 18;
  marcadorSelect:any = null;
  draggable:number=1;

  constructor(public _ms:MapasService){
      this._ms.cargarMarcadores();
  }
  clickMapa( event ) {
    console.log(event);
    let marcador:Marcador = {
      lat:event.coords.lat,
      lng:event.coords.lng,
      titulo: 'Nuevo',
      desc:'',
      draggable:true
    }

    this._ms.agregarMarcador(marcador);
  }

  clickMarcador(marcador:Marcador, id:number){
    console.log(marcador);
    console.log(id);
    this.marcadorSelect = marcador;
    this.draggable = this.marcadorSelect.draggable == true ? 1 : 0;
  }
  dragEndMarcador( marcador:Marcador, event ) {
    console.log(marcador);
    console.log(event);

    marcador.lat = event.coords.lat;
    marcador.lng = event.coords.lng;

    this._ms.guardarMarcadores();
  }
  borrarMarcador(id:number){
    this.marcadorSelect = null;
    this._ms.borrarMarcador(id);
  }
  cambiarDraggable(){

    let bool_value = this.draggable == 1 ? true : false;
  //  console.log(bool_value);
    this.marcadorSelect.draggable = bool_value;
    // if(this.draggable == 0 ){
    //     this.marcadorSelect.draggable = false;
    // }
    // else {
    //   this.marcadorSelect.draggable = true;
    // }

  }

}
