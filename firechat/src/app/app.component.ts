import { Component } from '@angular/core';
// import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { ChatService } from './services/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  // chats: FirebaseListObservable<any[]>;
  // constructor(af: AngularFire) {
  //   this.chats = af.database.list('/chats');
  // }
  constructor(public _cs:ChatService) {
    
  }
}
