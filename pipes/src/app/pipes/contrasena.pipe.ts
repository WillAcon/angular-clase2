import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: any, activar:boolean=true): any {

       if(activar){
         let password:string = "";
          for(let i= 0 ; i<value.length ; i++){
            password += "*";
          }
         return password;
       }else {
         return value;
       }
  }

}
