import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  nombre = "Williams";
  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI = Math.PI;

  a= 0.2344533;

  salario = 1234.5;

  heroe = {
    nombre: "Logan",
    clave: "Wolverine",
    edad: 500,
    direccion: {
      calle: "Primera",
      casa: "19"
    }
  };

  valordePromesa = new Promise((resolve, reject)=>{
    setTimeout(() => resolve('Llego la data'), 3500);
  });

  fecha = new Date();

  nombre2 = "Williams acUña conYas";

  video =  "u9jjbnGCK0w";
  activar:boolean = true;
  clave = "password111";

}
