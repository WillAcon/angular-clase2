import { Component, OnInit } from '@angular/core';
import {HeroesService, Heroe} from '../../services/heroes.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {
  heroes:Heroe[] = [];
  termino:string;
  constructor(private _heroesService:HeroesService,
              private activatedRoute:ActivatedRoute
            ) {
      this.activatedRoute.params.subscribe( params => {
        this.termino = params['name'];
        this.heroes = this._heroesService.buscarHeroes(params['name']);
      });
 }

  ngOnInit() {
  }

  loadHeroes(){
   //this.heroes =  this._heroesService.buscarHeroes(termino);
   //console.log(this.heroes);

   //this.router.navigate(['/search', termino])
  }
}
