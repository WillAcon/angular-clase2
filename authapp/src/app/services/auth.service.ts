import { Injectable }      from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';

import { Router } from '@angular/router';
// Avoid name not found warnings
declare var Auth0Lock: any;

@Injectable()
export class Auth {

  //profile user
  userProfile: Object;
  //opciones
  opciones:Object = {
    allowedConnections: ["Username-Password-Authentication","facebook","twitter"],
    rememberLastLogin: false,
    socialButtonStyle: "small",
    languageDictionary: {"title":"Auth demo"},
    language: "es",
    theme: {"logo":"https://cdn2.auth0.com/website/playground/schneider.svg","primaryColor":"#3A99D8"},
    additionalSignUpFields: [{
        name: "direccion",
        placeholder: "Ingrese su dirección",
        // The following properties are optional
        icon: "https://static.xx.fbcdn.net/rsrc.php/v3/yc/r/0mDg6gluHrS.png",

        validator: function(direccion) {
          return {
             valid: direccion.length >= 10,
             hint: "La dirección tiene que ser mayor a 10 caracteres" // optional
          };
        }
      },
      {
        name: "nombre_completo",
        placeholder: "Ingrese su nombre",
        icon:"http://pavilionchurch.org.uk/Images/Content/2155/Templates/36085/images/icon_user.png"
    }]
  };

  // Configure Auth0
  lock = new Auth0Lock('xOS4QWchZ7gMxwhS1MXlDoIw1WkF7YUg', 'willacon7.auth0.com', this.opciones);

  constructor(
    private router:Router
  ) {
    // Add callback for lock `authenticated` event
    this.lock.on("authenticated", (authResult) => {
      localStorage.setItem('id_token', authResult.idToken);

      // Fetch profile information
        this.lock.getProfile(authResult.idToken, (error, profile) => {
          if (error) {
            // Handle error
            alert(error);
            return;
          }

          localStorage.setItem('profile', JSON.stringify(profile));
          this.userProfile = profile;
        });


    });

  }

  public getProfile(){
    if(this.authenticated()) {
      return JSON.parse(localStorage.getItem('profile'));
    }
    else{
      return {};
    }
  }

  public login() {
    // Call the show method to display the widget.
    this.lock.show();
  }

  public authenticated() {
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired('id_token');
  }

  public logout() {
    // Remove token from localStorage
    this.router.navigate(['/home']);
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
  }
}
