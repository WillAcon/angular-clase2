import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../services/peliculas.service';
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styles: []
})
export class PeliculaComponent implements OnInit {
  pelicula:any;
  page:string;
  constructor(public _ps:PeliculasService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( parametros => {
      this.page = parametros.page;
      if(parametros['id']){
        this.getPelicula(parametros['id']);
      }
    });
  }

  ngOnInit() {
  }

  getPelicula(id:string) {
    this._ps.getPelicula(id)
            .subscribe(
              data=> {this.pelicula = data; console.log(data);}
            )
  }

}
