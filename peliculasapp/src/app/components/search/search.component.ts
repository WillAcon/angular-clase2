import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../services/peliculas.service';
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {
  buscar:string = "";
  constructor(public _ps:PeliculasService, private route:ActivatedRoute, private router:Router) {
    this.route.params.subscribe( parametros => {
      console.log(parametros);
      if(parametros['texto']){
        this.buscar = parametros['texto'];
        this.buscarPelicula();
      }
    });
  }

  ngOnInit() {
  }
  buscarPelicula(){
    if(this.buscar.length == 0) {
      return;
    }
  //  this.router.navigate(['/search',this.buscar]);
   this._ps.buscarPelicula(this.buscar)
           .subscribe()

  }
  buscarPeli() {
    this._ps.buscarPelicula(this.buscar)
             .subscribe()
  }

}
